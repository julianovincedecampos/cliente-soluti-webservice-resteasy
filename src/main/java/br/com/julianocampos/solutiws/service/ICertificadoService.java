package br.com.julianocampos.solutiws.service;

import java.util.List;

import javax.websocket.server.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.soluti.julianocampos.model.Certificado;

@Path("/certificado")
public interface ICertificadoService {
	@GET
	@Path("/listar")
	@Produces("application/json")
	public List<Certificado> getCertificados();

	@POST
	@Path("/gravar")
	@Consumes("application/json")
	public Response gravar(Certificado certificado);

	@DELETE
	@Path("/delete/{certificadoId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteCertificado(@QueryParam("certificadoId") int certificadoId);

	@GET
	@Path("/detalhes/{certificadoId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Certificado getCertificados(@QueryParam("certificadoId") Integer certificadoId);
}
