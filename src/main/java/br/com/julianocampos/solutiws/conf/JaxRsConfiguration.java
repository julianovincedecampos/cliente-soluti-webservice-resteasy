package br.com.julianocampos.solutiws.conf;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/services")
public class JaxRsConfiguration extends Application{

}
