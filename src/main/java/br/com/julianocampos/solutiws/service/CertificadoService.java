package br.com.julianocampos.solutiws.service;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import br.com.julianocampos.solutiws.dao.CertificadoDao;
import br.com.soluti.julianocampos.model.Certificado;

@Path("/certificado")
public class CertificadoService implements ICertificadoService {

	@Inject
	private CertificadoDao certificadoDao;

	public List<Certificado> getCertificados() {
		return certificadoDao.lista();
	}

	public Response gravar(Certificado certificado) {
		certificadoDao.gravar(certificado);
		String result = "Certificado criado : " + certificado;
		return Response.status(201).entity(result).build();
	}

	public Response deleteCertificado(@QueryParam("certificadoId") int certificadoId) {
		certificadoDao.excluir(certificadoId);
		String result = "Certificado excluido : " + certificadoId;
		return Response.status(201).entity(result).build();
	}

	public Certificado getCertificados(Integer certificadoId) {
		return certificadoDao.buscaPorId(certificadoId);
	}

}
