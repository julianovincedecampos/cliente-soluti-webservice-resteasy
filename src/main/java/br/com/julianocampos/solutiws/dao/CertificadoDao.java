package br.com.julianocampos.solutiws.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.soluti.julianocampos.model.Certificado;

@Stateless
public class CertificadoDao {
	@PersistenceContext
	private EntityManager entityManager;
	
	public List<Certificado> lista(){
		String jpql = "select c from Certificado c ORDER BY c.id DESC";
		return entityManager.createQuery(jpql,Certificado.class).getResultList();
	}

	public String excluir(int certificadoId) {
		Certificado certificado = entityManager.find(Certificado.class,certificadoId);
		entityManager.remove(certificado);
		return certificado.toString();
	}
	
	public void gravar(Certificado certificado) {
		entityManager.persist(certificado);
	}

	public Certificado buscaPorId(Integer certificadoId) {
		return entityManager.find(Certificado.class,certificadoId);
	}
}
